import os                           # importa biblioteca do sistema operacional para verificar os arquivos
from datetime import datetime       # importa funcao datatime da biblioteca datatime para pegar data e hora atuais

class Error(Exception):     #Criando uma classe de exeções
    pass                    #passa por essa linha sem fazer nada (para respeitar a identacao) 
                        
def Verifica_CLiente():                             #Funcao recebe CPF e senha e verifica se esta tudo correto
    try:                                            #Tenta executar os comandos dentro do bloco, caso algum erro ocorra vai para as exeções
        CPF=int(input('Digite o CPF: '))            #Recebe o CPF e armazena como um numero inteiro
        Senha=int(input('Digite a senha: '))        #Recebe a senha e armazena como um numero inteiro

        if (Senha == Dados[CPF][3]):                #Verifica se a senha digitada é igual a senha no banco de dados
            return(CPF)                             #Retorna o CPF para ser utilizado
        
        print('\nUsuario/Senha nao conferem')       #Caso nao entrar no IF significa que a senha esta errado e mostra a mensagem
        raise Error()                               #Aciona a execao "Error" da funcao anterior levando para o final da operacao
    
    except KeyError:                                #Caso a biblioteca nao escontre a chave aciona essa execao
        print('\nCPF não encontrado')               #Print uma mensagem que o cpf nao foi encontrado
        raise Error()                               #Aciona a execao "Error" da funcao anterior levando para o final da operacao
        
    except ValueError:                              #Caso acontecer um erro devido o formato do valor inserido aciona essa execao 
        print('\nEntrada Invalida')                 #Print uma mensagem de erro
        raise Error()                               #Aciona a execao "Error" da funcao anterior levando para o final da operacao

def Checar_CPF(CPF):                #Checar se o CPF digitado segue é verdadeiro
    soma=0                          #Zera variavel para armazenar uma soma                   #Transforma CPF em string para trabalhar com cada digito
    if len(CPF) == 11:                #Verifica se o CPF possui todos os 11 digitos
        for i in range(11):         #For para varrer cada digito do CPF
            soma += int(CPF[i])       #Soma cada digito do CPF

        soma=str(soma)              #Transforma o resultado da soma em string para manipular
        if soma[0] == soma[1]:      #Verifica se os dois digitos do resultado são iguas
            return                  #Ao passar em todos os testes retorna normalmente
    
    print('\nCPF invalido')         #Se não passar pelos testes chega nesse ponto e mostra a mensagem
    raise Error()                   #Ativa a execao "Error" da funcao anterior levando para o final da operacao
    
def Salvar(x):                                  #Recebe a variavel com o dicionario e salva em um arquivo txt
    Info = open('Dados Bancarios.txt','w')      #Abre / Cria um txt chamado Dados Bancarios no modo escrita na variavel Info
    Info.write(str(x))                          #Escreve o conteudo do dicionario no formato string no txt 
    Info.close                                  #Fecha o arquivo para concluir o salvamento
    
def Carregar():                                         #Carrega os dados do txt e retorna atraves da funcao
    if (not(os.path.isfile('Dados Bancarios.txt'))):    #Verifica se o NAO arquivo existe
        return({})                                      #Se NAO existe retorna um dicionario vazio
    
    Info = open('Dados Bancarios.txt','r')              #Se existe abre o txt chamado Dados Bancarios no modo leitura na variavel Info
    x = Info.read()                                     #Passa as informacoes do txt para a variavel "x"
    Info.close()                                        #Fecha o arquivo txt
    return eval (x)                                     #Retorna as informacoes da biblioteca (eval e utilizado para acertar os formatos
                                                        # da informacoes dentro do txt que viraram string durante a funcao salvar() )
  
def Novo_Cliente():                                                                         #Cadastra um novo cliente no sistema
    try:                                                                                    #Tenta executar bloco abaixo caso aparece algum erro vai direto para a execoes
        Nome=input('Digite o nome: ')                                                       #Recebe o nome do usuario 
        CPF=input('Digite o CPF: ')                                                         #Recebe o CPF como uma string
        Checar_CPF(CPF)                                                                     #Vai para funcao passando a variavel CPF
        CPF = int(CPF)                                                                      #Transforma a string CPF em um inteiro
        if CPF in Dados:                                                                    #Verifica se o CPF ja existe
            print('\nCPF ja cadastrado')                                                    #Se o CPF existe Mostra a mensagem
            return                                                                          #Finaliza a funcao e volta para o menu
        T_Conta=input('Digite o tipo de conta (S = Salario, C = Comum e P = Plus): ')       #Recebe o tipo de conta 
        T_Conta=T_Conta.upper()                                                             #Transforma a string T_Conta em maiusculo
        
        if T_Conta != 'S' and T_Conta != 'C' and T_Conta !='P':                                                  #Verifica se NAO e um dos possiveis tipos de conta
            print ('\nTipo de conta invalida')                                              #Se Nao for mostra a mensagem
            return                                                                          #Finaliza a funcao
        
        Val_ini=float(input('Digite o valor inicial da conta: '))                             #Recebe o valor inicial da conta e salva como inteiro
        if Val_ini < 0:                                                                     #Verifica se o valor e negativo
            raise ValueError                                                                #Caso for negativa vai para execao ValueError                     
        Senha=int(input('Digite a senha: '))                                                #Recebe o valor da senha e salva como inteiro 
            
        Dados[CPF]=[Nome,T_Conta,Val_ini,Senha,[]]                                          #Armazena o novo cliente na biblioteca utilizandoi o CPF como chave
        
    except ValueError:                                                                      #Execao que ocorre quando algumas das entradas nao é o formato esperado
        print('\nEntrada Invalida')                                                         #Mostra a mensagem de erro
        
    except Error:                                                                           #Execao criada e que pode ser acionado pela funcao Chega_CPF() 
        pass                                                                                #Passa por essa linha sem fazer nada (para respeitar a identacao) 
        
def Apaga_Cliente():                                                                        #Apaga um cliente 
     
    try:                                                                                    #Tenta executar o bloco abaixo caso algum erro vai para a execao 
        CPF=Verifica_CLiente()                                                              #Chama a funcao Verifica_Cliente() caso estiver tudo certo vai retornar o CPF
        Dados.pop(CPF)                                                                      #Atraves do comando pop() remove do dicionario tudo da chave dentro da variavel CPF
        print('\nUsuario apagado com sucesso')                                              #Mostra uma mensagem de feedback
    
    except Error:                                                                           #Caso a funcao Verifica_Cliente() ativar a execao Error o codigo volta direto para esse ponto
        pass                                                                                #Passa por essa linha sem fazer nada (para respeitar a identacao) 

def Debita():                                                       #Realiza operacao de debito 
    
    try:                                                            #Tenta executar o bloco abaixo caso algum erro vai para a execao 
        CPF=Verifica_CLiente()                                      #Chama a funcao Verifica_CLiente() caso estiver tudo certo vai retornar o CPF
        Valor=float(input('Digite o valor a ser debitado: '))       #Recebe o valor que sera debitado
        if Valor < 0:                                               #Verifica se o valor e negativo
            raise ValueError                                        #Se for negativo vai para excecao ValueError
        Taxa={'S':0.05,'C':0.03,'P':0.01}                           #Criando uma Biblioteca para os tipos de conta e suas taxas
        V_Taxa=Valor*Taxa[ Dados[CPF][1] ]                          #Calcula a taxa desse debito atraves da biblioteca Taxas em conjunto com a informacao do tipo de conta na biblioteca dados
        V_Taxa=round(V_Taxa,2)                                      #Arredondando o valor encontrado para 2 casas decimais
        N_Saldo=Dados[CPF][2]-(Valor + V_Taxa)                      #Calcula o novo saldo da conta
        
        if (N_Saldo < 0 and Dados[CPF][1] == 'S'):                  #Verifica se o novo saldo é menor que zero e a conta e salario
            print ('\nSaldo indisponivel')                          #Caso for mostra mensagem de erro
        elif (N_Saldo < -500 and Dados[CPF][1] == 'C'):             #Verifica se o novo saldo é menor que -500 e a conta e comum
            print ('\nSaldo indisponivel')                          #Caso for mostra mensagem de erro
        elif (N_Saldo < -5000):                                     #Verifica se o novo saldo é menor que -5 mil  
            print ('\nSaldo indisponivel')                          #Caso for mostra mensagem de erro
        else:                                                       #Caso passar por todos os testes conclui a operacao
            print ('\nOperacao realizada com sucesso')              #Mostra mensagem de conclusao
            print ('Saldo resultante: R$ %.2f' %(N_Saldo))          #Mostra o novo saldo
            Dados[CPF][2] = N_Saldo                                 #Salva o novo saldo na biblioteca dados
            
            Hora = datetime.now()                                   #Utiliza funcao para obter data e hora atuais como uma string
            Hora = Hora.strftime('%d-%m-%Y %H:%M')                  #Altera a formatacao da string Hora para o formato desejado
            
            Valor = ('%.2f' %(Valor))                               #Passa a informacao do Valor para uma string com 2 casas decimais
            for x in range ( 8 - len(Valor) ):                      #Verifica o tamando da string e realiza um for na range de 7 - o tamnho
                Valor = ' ' + Valor                                 #Adiciona espaços em branco no inicio ate que a string fique com o tamanho == 7 
                
            V_Taxa = ('%.2f' %(V_Taxa))                             #Padroniza o tamanho da informacao da taxa idem realizado com o Valor
            for x in range ( 5 - len(Valor) ):                      # Com o tamanho == 5
                V_Taxa = ' ' + V_Taxa
            
            Dados[CPF][4].append('Data: %s  - %s  Tarifa: %s  Saldo: %.2f' %(Hora,Valor,V_Taxa,N_Saldo))  #Salva uma String com o extrato da operacao realizada na biblioteca dados
            
    except ValueError:                                              #Caso a funcao Verifica_Cliente() ativar a execao Error o codigo volta direto para esse ponto
        print('\nEntrada Invalida')                                 #Mostra a mensagem de erro            

    except Error:                                                   #Execao criada e que pode ser acionado pela funcao Chega_CPF() 
        pass                                                        #Passa por essa linha sem fazer nada (para respeitar a identacao) 

def Deposita():                                                         #Funcao para realizar deposito
    try:                                                                #Tenta executar o bloco abaixo caso algum erro vai para a execao 
        CPF=Verifica_CLiente()                                          #Chama a funcao Verifica_CLiente() caso estiver tudo certo vai retornar o CPF
        Valor=float(input('Digite o valor a ser depositado: '))         #Recebe o valor que sera depositado
        if Valor < 0:                                                   #Verifica se o valor e negativo                        
            raise ValueError                                            #Se for negativo vai para excecao ValueError
        N_Saldo=Dados[CPF][2]+Valor                                     #Adiciona o valor depositado com o valor atual da conta e salva 

        print ('\nOperacao realizada com sucesso')                      #Mostra uma mensagem de feedback
        print ('Saldo resultante: R$ %.2f' %(N_Saldo))                  #Mostra o novo saldo
        
        Dados[CPF][2] = N_Saldo                                         #Salva o novo saldo na biblioteca dados
            
        Hora = datetime.now()                                           #Idem funcao debita
        Hora = Hora.strftime('%d-%m-%Y %H:%M')
        
        Valor = ('%.2f' %(Valor))                                       #Idem funcao debita
        for x in range ( 8 - len(Valor) ):
            Valor = ' ' + Valor
        
        Dados[CPF][4].append('Data: %s  + %s  Tarifa:  0.00  Saldo: %.2f' %(Hora,Valor,N_Saldo))     #Idem funcao debita, porem a tarifa e fixa em zero
        
    except ValueError:                                                  #Idem funcoes anteriores
        print('\nEntrada Invalida')

    except Error:                                                       #Idem funcoes anteriores
        pass

def Saldo():                                                    #Funcao para verificacao do saldo de uma conta
    try:                                                        #Tenta executar o bloco abaixo caso algum erro vai para a execao 
        CPF=Verifica_CLiente()                                  #Chama a funcao Verifica_CLiente() caso estiver tudo certo vai retornar o CPF
        print ('\nSaldo atual: R$ %.2f' %(Dados[CPF][2]))       #Mostra o Saldo da conta

    except Error:                                               #Idem funcoes anteriores
        pass    
    
def Extrato():                                                                                          #Funcao para mostrar o extrato de operacoes de uma conta
    try:                                                                                                #Tenta executar o bloco abaixo caso algum erro vai para a execao 
        CPF=Verifica_CLiente()                                                                          #Chama a funcao Verifica_CLiente() caso estiver tudo certo vai retornar o CPF
        T_Contas={'S':'Salario','C':'Comum','P':'Plus'}                                                 #Cria uma biblioteca para o nome dos tipos de contas
        print('\nNome: %s\nCPF: %d\nConta: %s' %(Dados[CPF][0], CPF, T_Contas[ Dados[CPF][1] ] ) )      #Mostra as informacoes da conta no inicio do extrato
        for i in range(len(Dados[CPF][4])):                                                             #for para mostrar todos os campos do vetor do extrato 
            print(Dados[CPF][4][i])                                                                     #Mostras dados de cada operacoes conforme o "i" varia com o for
    
    except Error:                                                                                       # Idem funcao anterior
        pass  

# FIM das funcoes e inicio do programa principal

Op=1                                                        #Valor inicial para que o programa entre no while
while(Op!=0):                                               #Loop principal do programa. Caso a operacao selecionada for 0 sai do while e encerra o codigo
    
    Dados = Carregar()                                      #Vai para a funcao de carregar dados do txt e salva na variavel Dados1
    
    try:                                                    #Tenta executar o bloco abaixo caso algum erro vai para a execao 
        print('\n1 - Novo Cliente')                     
        print('2 - Apaga Cliente')
        print('3 - Debita')
        print('4 - Deposita')                               #Mostra o menu
        print('5 - Saldo')
        print('6 - Extrato \n')
        print('0 - Sair')
    
        Op=int(input("Digite a opcao desejada: "))          #Recebe a operacao como um numero inteiro 
        
        if (Op == 1):                                           
            Novo_Cliente()
        elif (Op == 2):
            Apaga_Cliente()
        elif (Op == 3):
            Debita()
        elif (Op == 4):                                     #Cada numero de Op chama sua respectiva funcao conforme o menu
            Deposita()
        elif (Op == 5):
            Saldo()
        elif (Op == 6):
            Extrato()
        elif (Op != 0):                                     #Caso um numero fora das opcoes do menu for inserido vai mostrar o erro
            print('\nOpcao Invalida')                       # e o menu vai aparecer novamente
                                                            
    except ValueError:                                      #Excesao acionada caso o usuario insira algo que nao seja um numero inteiro
        print('\nEntrada Invalida')                         #Mostra o erro
        
    Salvar(Dados)                                           #Vai para a funcao que salva a biblioteca Dados no txt   